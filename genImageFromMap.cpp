#include <SFML/Graphics.hpp>


void genImageFromMap(sf::Image* image, float** map)
{
    for (unsigned int i = 0; i < image->getSize().y; i++)
    {
        for (unsigned int j = 0; j < image->getSize().x; j++)
        {
            auto h = static_cast<sf::Uint8>(map[i][j] * 255);
            sf::Color color(h, h, h);


            if
                (h > 255 * (0.9f))
            {
                color = sf::Color(h, h, h);
            }
            else if
                (h > 255 * (4 / 5.0f))
            {
                color = sf::Color(h - 80, h - 120, 50);
            }
            else if
                (h > 255 * (2 / 3.0f))
            {
                color = sf::Color(0, h - 100, 0);
            }
            else
            {
                color = sf::Color(20, 0, h * 0.5f + 32);
            }

            image->setPixel(j, i, color);
        }
    }
}