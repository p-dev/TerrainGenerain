//
// DiamondSquare
//

#include <cstdlib>
#include "rand-adv.h"
#include "DiamondSquare.h"
#include "../../../../../usr/lib/gcc/i686-w64-mingw32/5.3-win32/include/c++/iostream"


template <typename T>
void limit(T* num, T min, T max)
{
    if (*num > max)
    {
        *num = max;
    }
    if (*num < min)
    {
        *num = min;
    }
}



DiamondSquare::DiamondSquare(size_t width, size_t height, float z_scale)
    :
    _height(height),
    _width(width),
    _z_scale(z_scale)
{
    _map = new float*[_height];
    for (int i = 0; i < _height; i++)
    {
        _map[i] = new float[_width];
    }
}


DiamondSquare::~DiamondSquare()
{
    for (int i = 0; i < _height; i++)
    {
        delete _map[i];
    }
    delete _map;
}


float** DiamondSquare::getMatrix()
{
    return _map;
}


void DiamondSquare::generate()
{
    _map[    0    ][   0    ] = randRange(0.0f, _z_scale);
    _map[    0    ][_width-1] = randRange(0.0f, _z_scale);
    _map[_height-1][   0    ] = randRange(0.0f, _z_scale);
    _map[_height-1][_width-1] = randRange(0.0f, _z_scale);

    squareStep(_width / 2);

    normalise();
}


void DiamondSquare::squareStep(int n)
{
    if (n != 0)
    {
        for (int i = n; i < _height; i += n * 2)
        {
            for (int j = n; j < _width; j += n * 2)
            {
                _map[i][j] = ((
                        _map[i + n][j + n] +
                        _map[i + n][j - n] +
                        _map[i - n][j + n] +
                        _map[i - n][j - n]
                    ) / 4.0f) + randRange(-_z_scale, _z_scale) * ( n / static_cast<float>(_width));
            }
        }
        diamondStep(n);
    }
}


void DiamondSquare::diamondStep(int n)
{
    for (int i = 0; i < _height; i += n)
    {
        for (int j = (1 - ((i / n) % 2)) * n; j < _width; j += n * 2)
        {
            float div = 0.0f;
            float sum = 0.0f;

            if ((i - n) >= 0)
            {
                sum += _map[i - n][j];
                div++;
            }
            if ((i + n) < _height)
            {
                sum += _map[i + n][j];
                div++;
            }
            if ((j - n) >= 0)
            {
                sum += _map[i][j - n];
                div++;
            }
            if ((j + n) < _width)
            {
                sum += _map[i][j + n];
                div++;
            }
            _map[i][j] = sum / div + randRange(-_z_scale, _z_scale) * (n / static_cast<float>(_width));
        }
    }

    squareStep(n / 2);
}


void DiamondSquare::normalise()
{
    float min = _map[0][0];
    float max = _map[0][0];

    // Find min/max
    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            if (_map[i][j] < min) { min = _map[i][j]; }
            if (_map[i][j] > max) { max = _map[i][j]; }
        }
    }

    // Normalise
    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            _map[i][j] = (_map[i][j] - min) / (max - min);
        }
    }
}


void DiamondSquare::print()
{
    for (int i = 0; i < _height; i++)
    {
        for (int j = 0; j < _width; j++)
        {
            std::cout << _map[i][j] << " ";
        }
        std::cout << std::endl;
    }
}


size_t DiamondSquare::getWidth()
{
    return _width;
}


size_t DiamondSquare::getHeight()
{
    return _height;
}
