#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtx/transform.hpp>

#include "Camera.h"
#include "DiamondSquare.h"
#include "GLLoadShaders.h"


// Global
static int  g_gl_err_counter        = 0;

float       g_time          = 0.0f;



void E_chkGlErr()
{
    std::cout << "E" << g_gl_err_counter++ << " " << glGetError() << std::endl;
}


glm::vec3 vecAvg(size_t count, glm::vec3 norms ...)
{
    glm::vec3 vec_sum = glm::vec3();
    for (int i = 0; i < count; i++)
    {
        vec_sum += *(&norms + i * sizeof(norms));
    }
    return vec_sum / (float)count;
}


glm::vec3 triangleNormal(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3)
{
    return glm::normalize(glm::cross((v2 - v1), (v3 - v1)));
}


void inWin3d(DiamondSquare& disq)
{
    GLuint MAP_H = disq.getHeight();
    GLuint MAP_W = disq.getWidth();

    if (!glfwInit()) { throw std::runtime_error("Failed to init GLFW."); }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(1024, 720, "DiSq Win3D", 0, 0);
    if (window == nullptr) { throw std::runtime_error("Failed to create window."); }
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) { throw std::runtime_error("Failed to init GLEW."); }


    GLuint shaderprogram = LoadShaders(
        "VertexShader.glsl",
        "FragmentShader.glsl"
    );
    glGetError();

    glUseProgram(shaderprogram);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


    // Light

    GLuint light_vec_loc = glGetUniformLocation(shaderprogram, "light_vec");
    glm::vec3 light_vec = -glm::vec3(-1.0f, -1.0f, -1.0f);
    glUniform3fv(light_vec_loc, 1, &light_vec[0]);


    // Map VAO

    GLuint map_vao;
    glGenVertexArrays(1, &map_vao);


    // Map VBO

    GLuint map_vbo;
    glGenBuffers(1, &map_vbo);

    // Data
    disq.generate();
    float** map = disq.getMatrix();
    GLfloat* map_vbo_data = new GLfloat[MAP_H * MAP_W * 3];
    for (int i = 0; i < MAP_H; i++)
    {
        for (int j = 0; j < MAP_W; j++)
        {
            map_vbo_data[(i * MAP_H + j) * 3 + 0] = j;
            map_vbo_data[(i * MAP_H + j) * 3 + 1] = map[i][j];
            map_vbo_data[(i * MAP_H + j) * 3 + 2] = i;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, map_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(map_vbo_data[0]), map_vbo_data, GL_STATIC_DRAW);
    delete map_vbo_data;
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);


    // Map EBO
    GLuint* map_ebos;
    glGenBuffers(MAP_H - 1, map_ebos);

    // Data
    for (int line = 0; line < (MAP_H - 1); line++)
    {
        GLuint* ebo_data = new GLuint[MAP_W * 2];
        for (int i = 0; i < MAP_W; i++)
        {
            ebo_data[i * 2 + 0] = line * MAP_W + i;
            ebo_data[i * 2 + 1] = line * MAP_W + i + MAP_W;

            std::cout << ebo_data[i * 2 + 0] << " ";
            std::cout << ebo_data[i * 2 + 1] << " ";
        }
        std::cout << std::endl;
    }


    // Map Normals

    GLuint map_normal_bo;
    glGenBuffers(1, &map_normal_bo);

    // Data
    GLfloat* map_normal_data = new GLfloat[MAP_H * MAP_W * 3];
    for (int i = 0; i < MAP_H; i++)
    {
        for (int j = 0; j < MAP_W; j++)
        {
            glm::vec3 normal;
            if (i == 0)
            {
                if (j == 0)
                {
                    normal = triangleNormal(glm::vec3(j, map[i][j], i),
                                            glm::vec3(j, map[i][j + 1], i),
                                            glm::vec3(j, map[i + 1][j], i));
                }
                else if (j == (MAP_W - 1))
                {
                    normal = vecAvg(2,
                        triangleNormal(map[i][j],
                                       map[i + 1][j - 1],
                                       map[i][j - 1]),
                        triangleNormal(map[i][j],
                                       map[i + 1][j - 1],
                                       map[i + 1][j])
                    );
                }
                else
                {
                    normal = vecAvg(3,
                        triangleNormal(map[i][j], map[i + 1][j - 1], map[i][j - 1]),
                        triangleNormal(map[i][j], map[i + 1][j - 1], map[i + 1][j]),
                        triangleNormal(map[i][j], map[i][j + 1],     map[i + 1][j])
                    );
                }
            }
            else if (i == (MAP_H - 1))
            {
                if (j == 0)
                {
                    normal = vecAvg(2,
                        triangleNormal(map[i][j], map[i - 1][j + 1], map[i - 1][j]),
                        triangleNormal(map[i][j], map[i - 1][j + 1], map[i][j + 1]);
                    );
                }
                else if (j == (MAP_W - 1))
                {
                    normal = triangleNormal(map[i][j], map[i][j - 1], map[i - 1][j]);
                }
                else
                {
                    normal = vecAvg(3,
                        triangleNormal(map[i][j], map[i - 1][j + 1], map[i][j + 1]),
                        triangleNormal(map[i][j], map[i - 1][j + 1], map[i - 1][j]),
                        triangleNormal(map[i][j], map[i][j - 1],     map[i][j + 1])
                    );
                }
            }
            else if (j == 0)
            {
                normal = vecAvg(3,
                    triangleNormal(map[i][j], map[i - 1][j + 1], map[i - 1][j]),
                    triangleNormal(map[i][j], map[i - 1][j + 1], map[i][j + 1]),
                    triangleNormal(map[i][j], map[i + 1][j],     map[i][j + 1]),
                )
            }
            else if (j == (MAP_W - 1))
            {
                normal = vecAvg(3,
                    triangleNormal(map[i][j], map[i + 1][j - 1], map[i + 1][j]),
                    triangleNormal(map[i][j], map[i + 1][j - 1], map[i][j + 1]),
                    triangleNormal(map[i][j], map[i - 1][j],     map[i][j + 1]),
                )
            }
            else
            {
                normal = vecAvg(6,
                    triangleNormal(map[i][j], map[i - 1][j],     map[i - 1][j + 1]),
                    triangleNormal(map[i][j], map[i][j + 1],     map[i - 1][j + 1]),
                    triangleNormal(map[i][j], map[i][j + 1],     map[i + 1][j]),
                    triangleNormal(map[i][j], map[i + 1][j - 1], map[i + 1][j]),
                    triangleNormal(map[i][j], map[i + 1][j - 1], map[i][j - 1]),
                    triangleNormal(map[i][j], map[i - 1][j],     map[i][j - 1]),
                )
            }

            map_normal_data[(i * MAP_H + j) * 3 + 0] = normal.x;
            map_normal_data[(i * MAP_H + j) * 3 + 1] = normal.y;
            map_normal_data[(i * MAP_H + j) * 3 + 2] = normal.z;
        }
    }
    glBindBuffer(GL_ARRAY_BUFFER, map_normal_bo);
    glBufferData(GL_ARRAY_BUFFER, MAP_H * MAP_W * 3 * sizeof(*map_normal_data), map_normal_data, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);


    // Camera

    Camera camera(window);
    camera.setViewName(const_cast<char*>("view"));
    camera.setProjectionName(const_cast<char*>("projection"));
    camera.setLookPoint({ 0.0f, 0.0f, 0.0f });
    camera.setPerspective(glm::radians(45.0f), 0.001f, 10000.0f);

    GLuint model_loc = glGetUniformLocation(shaderprogram, "model");
    glm::mat4 model = glm::mat4();
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, &model[0][0]);


    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);


    // Main loop

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        g_time = static_cast<float>(glfwGetTime() / 10.0f);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        camera.setPos({ glm::cos(g_time) * MAP_W,
                        (MAP_H + MAP_W) / 2.0f,
                        glm::sin(g_time) * MAP_H });


        // Draw Map

        glBindVertexArray(map_vao);
        for (int line = 0; line < (MAP_H - 1); line++)
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, map_ebos[line]);

            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glDrawElements(GL_TRIANGLE_STRIP, MAP_W * 2, GL_UNSIGNED_INT, nullptr);
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
        }


        glfwSwapBuffers(window);
    }


    // Clean


}