#include <SFML/Graphics.hpp>

#include "genImageFromMap.h"

#include "DiamondSquare.h"



void inWin2d(DiamondSquare& disq)
{
    sf::Texture texture;
    sf::Sprite sprite;

    sf::Image image;
    image.create(disq.getWidth(), disq.getHeight());

    sf::RenderWindow window(sf::VideoMode(1024, 720), "DiSq Win");

    while (window.isOpen())
    {
        sf::Event event{};
        while(window.pollEvent(event))
        {
            if ((event.type == sf::Event::Closed) ||
                (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)))
            {
                window.close();
            }
        }
        window.clear();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            disq.generate();
            float** map = disq.getMatrix();

            genImageFromMap(&image, map);

            texture.loadFromImage(image);
            sprite.setTexture(texture);
        }

        window.draw(sprite);

        window.display();
    }
}