//
// DiamondSquare
//

#pragma once

#include <cstddef>


class DiamondSquare
{
public:

    DiamondSquare(size_t width, size_t height, float z_scale);
    ~DiamondSquare();

    void generate();

    float** getMatrix();

    size_t getWidth();
    size_t getHeight();

    void print();

private:

    size_t _width;
    size_t _height;

    float _z_scale;

    float** _map;


    void diamondStep(int n);
    void squareStep(int n);

    void normalise();

};



