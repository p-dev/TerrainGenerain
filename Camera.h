#pragma once

#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>



class Camera
{
private:

	glm::vec3 cameraPos;
	glm::vec3 cameraTarget;
	glm::vec3 cameraDirection;
	glm::vec3 cameraRight;
	glm::vec3 cameraUp;
	glm::vec3 cameraFront;

	glm::mat4 view;
	glm::mat4 projection;

	GLint viewLoc;
	GLint projectionLoc;

	GLFWwindow* window;


	char* viewString;
	char* projectionString;

	int width;
	int height;

public:

#pragma region get

	glm::vec3 getPos();

#pragma endregion

#pragma region set

	void setPerspective(float fovYRad, float zNear, float zFar);

	void setOrtho(float left, float right, float bottom, float top);

	void setPos(float x, float y, float z);
	void setPos(glm::vec3 position);

	void setLookPoint(float x, float y, float z);
	void setLookPoint(glm::vec3 look);

	void setViewName(char* name);
	void setProjectionName(char* name);

#pragma endregion


	Camera() {}
	Camera(GLFWwindow* window);

	~Camera();
};

