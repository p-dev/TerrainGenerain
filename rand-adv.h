//
// rand
//

#pragma once

#include <cstdlib>


template <typename T>
T randRange(T min, T max)
{
    return rand() / static_cast<T>(RAND_MAX) * (max - min) + min;
}