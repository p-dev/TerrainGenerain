#include <iostream>

#include <SFML/Graphics.hpp>

#include "genImageFromMap.h"
#include "DiamondSquare.h"



void inFiles(DiamondSquare& disq, int count)
{
    sf::Image image;
    image.create(disq.getWidth(), disq.getHeight());

    for (int i = 0; i < count; i++)
    {
        disq.generate();
        float** map = disq.getMatrix();

        genImageFromMap(&image, map);

        std::string map_name = "map" + std::to_string(i) + ".png";

        image.saveToFile(map_name);

        std::cout << "Generated " << map_name << std::endl;
    }
}