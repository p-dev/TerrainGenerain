#include <iostream>
#include <ctime>
#include <cmath>

#include "ViewTypes.h"

#include "DiamondSquare.h"





int main()
{
    srand(time(0));

    // Map size
    int n;
    std::cout << "Enter a map size (n^2+1) (rec - 8, 9, 10) : ";
    std::cin >> n;
    size_t width = pow(2, n) + 1;
    size_t height = width;

    DiamondSquare diamondSquare(width, height, 1.0f);

    // Show mode
    int mode;
    std::cout << "Show in window (0) or save in files (1)? : ";
    std::cin >> mode;


    switch (mode)
    {
    case 0:

        int d;
        std::cout << "In 2D (0) or 3D (1)? : ";
        std::cin >> d;

        switch (d)
        {
        case 0:
            inWin2d(diamondSquare);
            break;
        case 1:
            inWin3d(diamondSquare);
            break;

        }
        break;

    case 1:

        int maps_count;
        std::cout << "Enter maps count: ";
        std::cin >> maps_count;

        inFiles(diamondSquare, maps_count);

        break;
    }



    return 0;
}
