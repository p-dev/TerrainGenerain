//
// ViewTypes
//

#pragma once

#include "DiamondSquare.h"


void inWin2d(DiamondSquare& disq);

void inWin3d(DiamondSquare& disq);

void inFiles(DiamondSquare& disq, int count);