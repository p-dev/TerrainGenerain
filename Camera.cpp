#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <iostream>

#include "Camera.h"

#pragma region get

glm::vec3 Camera::getPos()
{
	return cameraPos;
}

#pragma endregion


#pragma region set

void Camera::setPos(glm::vec3 position)
{
	cameraPos = position;
	view = glm::lookAt(cameraPos, cameraTarget, cameraUp);
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
}
void Camera::setPos(float x, float y, float z)
{
	setPos({ x, y, z });
}

void Camera::setViewName(char* name)
{
	viewString = name;
	GLint shaderprogram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &shaderprogram);
	projectionLoc = glGetUniformLocation(shaderprogram, viewString);
}

void Camera::setProjectionName(char* name)
{
	projectionString = name;
	GLint shaderprogram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &shaderprogram);
	projectionLoc = glGetUniformLocation(shaderprogram, projectionString);
}

#pragma endregion

void Camera::setLookPoint(glm::vec3 look)
{
	cameraTarget = look;
	view = glm::lookAt(cameraPos, cameraTarget, cameraUp);
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
}
void Camera::setLookPoint(float x, float y, float z)
{
	setLookPoint({ x, y, z });
}


void Camera::setPerspective(float fovYRad, float zNear, float zFar)
{
	projection = glm::perspective(fovYRad, width / static_cast<float>(height), zNear, zFar);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, &projection[0][0]);
}


void Camera::setOrtho(float left, float right, float bottom, float top)
{
	projection = glm::ortho(left, right, bottom, top);
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, &projection[0][0]);
}


Camera::Camera(GLFWwindow* window) : window(window)
{
	glfwGetWindowSize(window, &width, &height);

	cameraPos = { 0.0f, 0.0f, 0.0f };
	cameraFront = { 0.0f, 0.0f, -1.0f };
	cameraUp = { 0.0f, 1.0f, 0.0f };

	GLint shaderprogram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &shaderprogram);

	viewLoc			= glGetUniformLocation(shaderprogram, "view");
	projectionLoc	= glGetUniformLocation(shaderprogram, "projection");

	cameraUp = { 0.0f, 1.0f, 0.0f };

	setPos(0.0f, 0.0f, 0.0f);
	setLookPoint(0.0f, 0.0f, 0.0f);
	setPerspective(glm::radians(45.0f), 0.01f, 100.0f);
}


Camera::~Camera()
{
}
